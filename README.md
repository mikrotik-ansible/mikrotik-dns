# Mikrotik DNS configuration.

A Role allows configuraiton of Mikrotik RouterOS DNS client and server using Ansible.


Examples of usage with comments are in docs/

Big thanks to Martin Dulin for his role https://github.com/mikrotik-ansible/mikrotik-firewall.
His role gave me an idea how solve RouterOS configuration tasks.
